#!/usr/bin/env ruby

#Add your package managers here

$fedoraBased="dnf"
$rhelBased="yum"
$debianBased="apt-get"
$archLinux="pacman"
$voidLinux="xbps-install"
$sabayon="equo"
$opensuse="zypper"

#command binding methods for different package managers
def dnf()
  $installCmd = "dnf install"
  $reinstallCmd = "dnf reinstall"
  $searchCmd = "dnf search"
  $updateCmd = "dnf upgrade"
  $syncCmd = "dnf --refresh check-update"
  $syncANDupdateCmd = "dnf --refresh upgrade"
  $refreshSyncCmd = "dnf --refresh check-update"
  $removeCmd = "dnf remove"
  $recursiveRemoveCmd = "dnf remove"
  $checkUpdatesCmd = "dnf check-update"
  $cleanCmd = "dnf autoremove"
end

def yum()
  $installCmd = "yum install"
  $reinstallCmd = "yum reinstall"
  $searchCmd = "yum search"
  $updateCmd = "yum update"
  $syncCmd = "yum check-update"
  $syncANDupdateCmd = "yum check-update; yum update"
  $refreshSyncCmd = "yum check-update"
  $removeCmd = "yum remove"
  $recursiveRemoveCmd = "yum remove"
  $checkUpdatesCmd = "yum check-update"
  $cleanCmd = "yum autoremove"
end

def pacman()
  $installCmd = "pacman -S"
  $reinstallCmd = "pacman -S --force"
  $searchCmd = "pacman -Ss"
  $updateCmd = "pacman -Su"
  $syncCmd = "pacman -Sy"
  $syncANDupdateCmd = "pacman -Syu"
  $refreshSyncCmd = "pacman -Syy"
  $removeCmd = "pacman -R"
  $recursiveRemoveCmd = "pacman -Rdd"
  $checkUpdatesCmd = "checkupdates"
  $cleanCmd = "pacman -Rsn $(pacman -Qdtq)"
end

def apt_get()
  $installCmd = "apt install"
  $reinstallCmd = "apt install --reinstall"
  $searchCmd = "apt search"
  $updateCmd = "apt upgrade"
  $syncCmd = "apt update"
  $syncANDupdateCmd = "apt update; sudo apt upgrade"
  $refreshSyncCmd = "apt  update"
  $removeCmd = "apt  remove"
  $recursiveRemoveCmd = "apt remove --purge"
  $cleanCmd = "apt autoremove"
  end

def xbps_install()
  $installCmd = "xbps-install"
  $reinstallCmd = "xbps-install -f"
  $searchCmd = "xbps-query -Rs"
  $updateCmd = "xbps-install -u"
  $syncCmd = "xbps-install -S"
  $syncANDupdateCmd = "xbps-install -Su"
  $refreshSyncCmd = "xbps-install -f -S"
  $removeCmd = "xbps-remove"
  $recursiveRemoveCmd = "xbps-remove -f"
  $cleanCmd = "xbps-remove -O"
end

def equo()
  $installCmd = "equo i"
  $reinstallCmd = "equo i"
  $searchCmd = "equo s"
  $updateCmd = "equo u"
  $syncCmd = "equo up"
  $syncANDupdateCmd = "equo up && sudo equo u"
  $refreshSyncCmd = "equo update --force"
  $removeCmd = "equo rm"
  $recursiveRemoveCmd = "equo rm --norecursive"
  $cleanCmd = "equo cleanup"
end
