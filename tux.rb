#!/usr/bin/env ruby
require_relative 'which'
require_relative 'PkgMgrs'

$version="0.1"

def blink(text); colorize(text, 1); end

def Usage()
  puts
  puts blink("""
  Usage : tux [options] [arguments]

  Options:


  i, -i, add, get, -S, install        Install a package
  ri, -ri, reinstall                  Reinstall a package
  r, -R, rm, remove, delete           Remove a package
  -rf. -Rdd, force-remove             Force remove a package
  c, -c, clean                        Remove orphan packages
  s, -s, se, -Ss, search, find        Search remote repository
  sy, -Sy, sync, refresh              Sync local and remote repo
  u, up, -u, -Su, upgrade             Upgrade packages on the system
  su, -Syu, sup, syncup               Sync mirror and upgrade system
  -v, --version                       Show current version
  """)
end

def TuxVersion()

  puts blink("""
    tux v#{$version} - Universal package manager wrapper
    \u00A9 2015 Justin Moore
    """)
end

# Colored Output
def colorize(text, color_code)
  "\e[#{color_code}m#{text}\e[0m"
end

############ Main Script #################

args=ARGV
argsShift=[]

for i in args do
  argsShift.push(i)
end

argsShift.shift

for i in argsShift do
  pkgArgs = "#{pkgArgs} #{i}"
end

# Remove the first character of pkgArgs to make search strings work p

if ARGV[2] then
  pkgArgs[0] = ''
end

#package managers to check for
pkgMgrs = ["#{$fedoraBased}",
           "#{$rhelBased}",
           "#{$debianBased}",
           "#{$archLinux}",
           "#{$voidLinux}",
           "#{$sabayon}",
           "#{$opensuse}"]

#iterate over list of package managers
#pass this to which(), take result and set it to pkgMgr
for i in pkgMgrs do
  if which(i) then
    pkgMgr=i
    break
  end
end

# since some package managers have hyphens
# and since methods can't have hyphens,
# we take out any existing hyphens and replace them with underscores
pkgMgr = pkgMgr.tr('-', '_')

# check for and execute methods with the same name as pkgMgr contains
send(pkgMgr)


#pull variables from currently loaded method, e.g pacman()
install = "#{$installCmd} #{pkgArgs}"
reinstall = "#{$reinstallCmd} #{pkgArgs}"
search = "#{$searchCmd} #{pkgArgs}"
update = "#{$updateCmd} #{pkgArgs}"
remove = "#{$removeCmd} #{pkgArgs}"
force_remove = "#{$recursiveRemoveCmd} #{pkgArgs}"
sync = "#{$syncCmd}"
supdate = "#{$syncANDupdateCmd}"
clean = "#{$cleanCmd}"



case args[0]

# Install Options
when
  "i",
  "-i",
  "-S",
  "add",
  "get",
  "install"; system "sudo #{install}"

# re-install options
when
  "ri",
  "-ri",
  "reinstall"; system "sudo #{reinstall}"


# Search options
when
  "s",
  "se",
  "-s",
  "-Ss",
  "find",
  "search";  system "#{search}"

# Upgrade options (without syncing)
when
  "u",
  "up",
  "-u",
  "-Su",
  "upgrade";  system "sudo #{update}"

# Remove options
when
  "r",
  "rm",
  "-R",
  "remove",
  "delete";  system "sudo #{remove}"

when
  "-rf",
  "-Rdd",
  "force-remove"; system "sudo #{force_remove}"
# clean Options
when
  "c",
  "-c",
  "clean";
  puts
  print blink("Are you sure you want to clean package cache?(y/n): ")
  STDOUT.sync = true
  answer = STDIN.gets.chomp

  if "#{answer}" == "n"
    exit 0;
  elsif "#{answer}" == "y"
    system ("sudo #{clean}")
  else
    print "Invalid input: '#{answer}'"
  end

# Sync options
when
  "sy",
  "-Sy",
  "sync",
  "refresh";  system "sudo #{sync}"

# Update options (with syncing)
when
  "su",
  "sup",
  "-Syu",
  "syncup";  system "sudo #{supdate}"

when
  "-v",
  "--version"; TuxVersion()
else
  Usage()

end
